#|
  This file is a part of cl-physics project.
  Copyright (c) 2018 Dimitar Stanev (jimstanev@gmail.com)
|#

#|
  Author: Dimitar Stanev (jimstanev@gmail.com)
|#

;; (ql:quickload :matlisp)

(defsystem "cl-physics"
  :version "0.1.0"
  :author "Dimitar Stanev"
  :license "CC"
  :depends-on ("array-operations")
  :components ((:module "src"
                :components
                ((:file "cl-physics"))))
  :description ""
  :long-description
  #.(read-file-string
     (subpathname *load-pathname* "README.markdown"))
  :in-order-to ((test-op (test-op "cl-physics-test"))))
