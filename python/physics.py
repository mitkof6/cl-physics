import numpy as np
import algopy as alg
import matplotlib.pyplot as plt


class State:

    def __init__(self):
        self.q = {}
        self.u = {}
        self.u_dot = {}

    def set_q(self, component):
        self.q[component] = component.get_q

    def set_u(self, component):
        self.u[component] = component.get_u

    def set_u_dot(self, component):
        self.u_dot[component] = component.u

    def get_q(self, component):
        return self.q[component]

    def get_u(self, component):
        return self.u[component]


class Component:

    def __init__(self):
        self.q = np.array([0, 0, 0])
        self.u = np.array([0, 0, 0])

    def get_q(self):
        return self.q

    def get_u(self):
        return self.u


comp = Component()
comp.q = np.array([1, 2, 3])
comp.u = np.array([4, 5, 6])

state = State()
state.set_q(comp)
state.set_u(comp)
