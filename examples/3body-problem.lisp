(eval-when
    (:compile-toplevel  ; executed by the file compiler
     :load-toplevel     ; executed at load-time of the compiled file
     :execute)          ; executed whenever else
  (ql:quickload :cl-physics)
  (use-package :cl-physics))

;; parameterse and constants (scaled values)
(defparameter year 3.15e7 "Seconds in a year.")
(defparameter sun-mass 1.9885e30 "The mass of the sun in kg.")
(defparameter earth-mass 5.972e24 "The mass of the earth in kg.")
(defparameter sun-initial-position #(0 0 0) "Initial velocity of the sun.")
(defparameter sun-initial-velocity #(0 0 0) "Initial velocity of the sun.")
(defparameter earth-initial-position #(1.496e8 0 0) "Initial velocity of the earth.")
(defparameter earth-initial-velocity #(0 35000 0) "Initial velocity of the earth.")

;; define system and components
(defparameter system (make-instance 'system))
(defparameter sun (make-instance 'particle :mass sun-mass))
(defparameter earth (make-instance 'particle :mass earth-mass))

;; define problem
(add-to-system! system sun)
(add-to-system! system earth)
(defparameter state (initialize-system system))
(set-position! state sun sun-initial-position)
(set-velocity! state sun sun-initial-velocity)
(set-position! state earth earth-initial-position)
(set-velocity! state earth earth-initial-velocity)

;; apply forces
(defparameter gravity-sun-on-earth (gravity sun earth))
(apply-force! earth gravity-sun-on-earth)
(apply-force! sun (negate gravity-sun-on-earth))
(apply-force! sun gravity-sun-on-earth)
(format t "~F~%" (get-position state earth))
(format t "~F~%" (get-position-derivative state earth))
(format t "~F~%" (get-velocity-derivative state earth))
(format t "~F~%" (get-state-derivative state earth))
(format t "~F~%" (get-total-force state earth))
(format t "~F~%" (get-system-state state system))
(format t "~F~%" (get-system-state-derivative state system))

;; advance state
(defparameter dt 0.1 "Integration step.")
(defparameter state-history nil "Collect the state information.")
(loop for i below 10
      do (progn
           (advance-state! dt state system)
           (push (get-total-force state earth) state-history)))
