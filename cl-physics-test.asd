#|
  This file is a part of cl-physics project.
  Copyright (c) 2018 Dimitar Stanev (jimstanev@gmail.com)
|#

(defsystem "cl-physics-test"
  :defsystem-depends-on ("prove-asdf")
  :author "Dimitar Stanev"
  :license "CC"
  :depends-on ("cl-physics"
               "prove")
  :components ((:module "tests"
                :components
                ((:test-file "cl-physics"))))
  :description "Test system for cl-physics"
  :perform (test-op (op c) (symbol-call :prove-asdf :run-test-system c)))
