(defpackage cl-physics
  (:use :cl)
  (:import-from #:array-operations
                #:subvec
                #:vectorize
                #:each)
  (:export #:negate
           ;;
           #:state #:set-state!
           ;;
           #:system #:add-to-system!
                    #:initialize-system
                    #:get-system-position
                    #:get-system-velocity
                    #:get-system-state
                    #:get-system-position-derivative
                    #:get-system-velocity-derivative
                    #:get-system-state-derivative
           ;;
           #:component #:set-position!
                       #:set-velocity!
                       #:get-position
                       #:get-velocity
                       #:apply-force!
                       #:get-total-force
           ;;
           #:particle #:get-position-derivative
                      #:get-velocity-derivative
                      #:get-state-derivative
           ;;
           #:gravity
           ;;
           #:advance-state!))
(in-package :cl-physics)

;;;-----------------------------------------------------------------------------
;;; healper functions
;;;-----------------------------------------------------------------------------

(defun norm2-squared (p q)
  "Euclidian distance squared."
  (reduce #'+
          (map 'list
               (lambda (px qx) (expt (- px qx) 2))
               p q)))

(defun norm2 (p q)
  "Euclidian distance."
  (sqrt (norm2-squared p q)))

(defun negate (function)
  "Negate the result of a function that accepts a state object as its argument."
  (lambda (state) (each #'- (funcall function state))))

(defmacro collect-to-vector (function list) ; TODO reader macro
  "Concatenates the result of applying the function on the list into a vector."
  `(reduce (lambda (x y) (concatenate 'vector x y))
           (map 'list ,function ,list)))

;;;-----------------------------------------------------------------------------
;;; state
;;;-----------------------------------------------------------------------------

(defclass state ()
  ((ti
    :initform (error "Time (:time) not provided.")
    :initarg :time
    :accessor ti
    :documentation "The time instance.")
   (q
    :initform (error "Q (:q) not provided.")
    :initarg :q
    :accessor q
    :documentation "The position variable of the state.")
   (u
    :initform (error "U (:u) not provided.")
    :initarg :u
    :accessor u
    :documentation "The velocity variable of the state.")))

(defmethod set-state! ((state state) new-time new-state-vector)
  "Sets the state from vector $[q u]^T$."
  (with-slots (ti q u)
      state
    (let ((nq (array-total-size q))
          (nu (array-total-size u))
          (ns (array-total-size new-state-vector)))
      (assert (= (+ nq nu) ns)
            nil
            "Dimensions mismatch ~D != ~D~%" (+ nq nu) ns)
      (setf ti new-time)
      (setf q (subvec new-state-vector 0 nq))
      (setf u (subvec new-state-vector nq ns)))))

;;;-----------------------------------------------------------------------------
;;; system
;;;-----------------------------------------------------------------------------

(defclass system ()
  ((last-q-index
    :initform 0
    :accessor last-q-index
    :documentation "The index of the next position variable in the state.")
   (last-u-index
    :initform 0
    :accessor last-u-index
    :documentation "The index of the next velocity variable in the state.")
   (components
    :initform nil
    :accessor components
    :documentation "List of components added in this system.")))

(defgeneric add-to-system! (system component)
  (:documentation "Adds a component to the system."))

(defmethod initialize-system ((system system))
  "Initializes the system and returns the state."
  (with-slots (last-q-index last-u-index)
    system
    (make-instance 'state
                   :time 0
                   :q (make-array last-q-index :initial-element 'nil)
                   :u (make-array last-u-index :initial-element 'nil))))

(defmethod get-system-position ((state state) (system system))
  "Collects the position ($q$) of all components in the system."
  (with-slots (components)
      system
    (collect-to-vector (lambda (component)
                         (get-position state component))
                       components)))

(defmethod get-system-velocity ((state state) (system system))
  "Collects the velocity ($u$) of all components in the system."
  (with-slots (components)
      system
    (collect-to-vector (lambda (component)
                         (get-velocity state component))
                       components)))

(defmethod get-system-state ((state state) (system system))
  "Collects the state ($x = [q u]^T$) of all components in the system."
  (concatenate 'vector
               (get-system-position state system)
               (get-system-velocity state system)))

(defmethod get-system-position-derivative ((state state) (system system))
  "Calculates the position derivative ($qdot = f_q(x)$) of all components in the
  system."
  (with-slots (components)
      system
    (collect-to-vector (lambda (component)
                         (get-position-derivative state component))
                       components)))

(defmethod get-system-velocity-derivative ((state state) (system system))
  "Calculates the velocity derivative ($udot = f_u(x)$) of all components in the
  system."
  (with-slots (components)
      system
    (collect-to-vector (lambda (component)
                         (get-velocity-derivative state component))
                       components)))

(defmethod get-system-state-derivative ((state state) (system system))
  "Calculates the state derivative ($xdot = [qdot udot]^T$) of all components in
  the system."
  (concatenate 'vector
               (get-system-position-derivative state system)
               (get-system-velocity-derivative state system)))

;;;-----------------------------------------------------------------------------
;;; component
;;;-----------------------------------------------------------------------------

(defclass component ()
  ((nq
    :initform (error "Number of position variables not specified.")
    :accessor nq
    :documentation "Dimension of position variables.")
   (nu
    :initform (error "Number of velocity variables not specified.")
    :accessor nu
    :documentation "Dimension of velocity variables.")
   (q-index
    :initform nil
    :accessor q-index
    :documentation "The index of the position variable in the state.")
   (u-index
    :initform nil
    :accessor u-index
    :documentation "The index of the velocity variable in the state.")
  (forcing
    :initform nil
    :accessor forcing
    :documentation "A list containing the acting force objects.")))

(defgeneric get-position-derivative (state component)
  (:documentation
   "Calculates the position derivative ($qdot = f_q$) of the component."))

(defgeneric get-velocity-derivative (state component)
  (:documentation
   "Calculates the position derivative ($udot = f_u$) of the component."))

(defgeneric get-state-derivative (state component)
  (:documentation
   "Calculates the state derivative ($xdot = f(x)$) of the component."))

(defmethod add-to-system! ((system system) (component component))
  (with-slots (components last-q-index last-u-index)
      system
    (assert (not (member component components))
            (component)
            "Component ~S is already present in the system" component)
    (setf components (append components (list component)))
    (with-slots (nq q-index nu u-index)
        component
      (setf q-index last-q-index)
      (incf last-q-index nq)
      (setf u-index last-u-index)
      (incf last-u-index nu))))

(defmethod set-position! ((state state) (component component) position)
  "Sets the position state variable of the component from position vector."
  (with-slots (nq q-index)
      component
    (assert (= (array-total-size position) nq)
            (position nq)
            "Dimensions mismatch ~S != ~S" (array-total-size position) nq)
    (with-slots (q)
        state
      (loop for i from 0 below nq
            do (setf (aref q (+ q-index i)) (aref position i))))))

(defmethod set-velocity! ((state state) (component component) velocity)
  "Sets the velocity state variable of the component from velocity vector."
  (with-slots (nu u-index)
      component
    (assert (= (array-total-size velocity) nu)
            (velocity nu)
            "Dimensions mismatch ~S != ~S" (array-total-size velocity) nu)
    (with-slots (u)
        state
      (loop for i from 0 below nu
            do (setf (aref u (+ u-index i)) (aref velocity i))))))

(defmethod get-position ((state state) (component component))
  "Gets the position state variable of the component."
  (with-slots (nq q-index)
      component
    (with-slots (q)
        state
      (subvec q q-index (+ q-index nq)))))

(defmethod get-velocity ((state state) (component component))
  "Gets the velocity state variable of the component."
  (with-slots (nu u-index)
      component
    (with-slots (u)
        state
      (subvec u u-index (+ u-index nu)))))

(defmethod apply-force! ((component component) force)
  "Aplly a force object to the list of forcings."
  (with-slots (forcing)
      component
    (setf forcing (push force forcing))))

(defmethod get-total-force ((state state) (component component))
  "Calculates the total force that is acting on a component."
  (with-slots (forcing)
      component
    (reduce (lambda (x y) (each #'+ x y))
            (map 'list
                 (lambda (function) (funcall function state))
                 forcing))))

;;;-----------------------------------------------------------------------------
;;; particle
;;;-----------------------------------------------------------------------------

(defclass particle (component)
  ((nq
    :initform 3)
   (nu
    :initform 3)
   (mass
    :initform (error "Particle mass not specified.")
    :initarg :mass
    :accessor mass
    :documentation "The mass of the particle.")))

(defmethod get-position-derivative ((state state) (particle particle))
  "Calculates the state derivative ($qdot = [u]^T$) of the particle."
  (get-velocity state particle))

(defmethod get-velocity-derivative ((state state) (particle particle))
  "Calculates the velocity derivative ($udot = [f / m]^T$) of the particle."
  (with-slots (mass forcing)
      particle
    (let ((f (get-total-force state particle)))
      (vectorize (f) (/ f mass)))))

(defmethod get-state-derivative ((state state) (particle particle))
  "Calculates the state derivative ($xdot = [u, f / m]^T$) of the particle."
  (concatenate 'vector
               (get-position-derivative state particle)
               (get-velocity-derivative state particle)))

;;;-----------------------------------------------------------------------------
;;; forces
;;;-----------------------------------------------------------------------------

(defmethod gravity ((component1 component) (component2 component))
  "Gravitational force exerted by the first component on the second component.

   $\vec{f} = \frac{G m_1 m_2}{r^3} \vec{r}$
  "
  (lambda (state)
    (let* ((G 6.674e-11)
           (m1 (mass component1))
           (m2 (mass component2))
           (x1 (get-position state component1))
           (x2 (get-position state component2))
           (scalar-term (/ (* G m1 m2) (expt (norm2 x1 x2) 3))))
      (vectorize (x1 x2) (* scalar-term (- x1 x2))))))

;;;-----------------------------------------------------------------------------
;;; numerical integration
;;;-----------------------------------------------------------------------------

(defun euler-method (dt x0 dxdt)
  "Numerical integration step function that uses Euler method."
  (vectorize (x0 dxdt) (+ x0 (* dxdt dt))))

(defmethod advance-state! (dt (state state) (system system))
  (let* ((x0 (get-system-state state system))
         (dxdt (get-system-state-derivative state system))
         (x (euler-method dt x0 dxdt)))
   (set-state! state (+ (ti state) dt) x)))
